import Vue from 'vue'
import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios  from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'

// Auth base configuration some of this options
// can be override in method calls

export default {
  auth: bearer,
  http: axios,
  router: router,
  tokenDefaultName: 'auth-token',
  tokenStore: ['localStorage'],
  authRedirect: {
    path: '/login'
  },

  loginData: {
    url: 'auth/login',
    method: 'POST',
    redirect:  '/post',
    fetchUser: true,
  },

  logoutData: {
    url: 'auth/logout',
    method: 'POST',
    redirect:  '/login',
    makeRequest: true,
  },

  fetchData: {
    url: 'auth/me',
    method: 'POST',
    enabled: true,
    success(res) {
      let user = res.data.user;
      Vue.auth.user(user);
    }
  },

  refreshData: {
    url: 'auth/refresh',
    method: 'POST',
    enabled: true,
    interval: 30,
  },
}
