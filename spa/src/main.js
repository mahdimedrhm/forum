import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
import auth from './auth'
import VueAuth from '@websanova/vue-auth';

export const bus = new Vue();

axios.defaults.baseURL = `${process.env.VUE_APP_API_URL}/api/`;

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
Vue.router = router;


Vue.use(VueAxios, axios)
Vue.use(VueAuth, auth);

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
