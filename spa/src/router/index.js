import Vue from 'vue'
import VueRouter from 'vue-router'
import Post from '../views/post/Index.vue'
import AddPost from '../views/post/Add.vue'
import EditPost from '../views/post/Edit.vue'
import PostDetails from '../views/post/Details.vue'
import Profile from '../views/profile/AuthUserProfile.vue'
import UserProfile from '../views/profile/Profile.vue'
import Login from '../views/login/Login.vue'
import Register from '../views/login/Register.vue'
import Home from "../views/home/Home.vue"
import User from "../views/user/index.vue"
Vue.use(VueRouter)

  const routes = [
  
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      auth: true
    }
  },
  {
    path:'/post',
    name:'Post',
    component: Post,
    meta: {
      auth: true
    }
  },
  {
    path:'/post/add',
    name: AddPost,
    component: AddPost
  },
  {
    path:'/post/edit/:id',
    name:EditPost,
    component: EditPost
  },
  {
    path:'/post/details/:id',
    name:PostDetails,
    component:PostDetails
  },
  {
    path: '/profile',
    name: Profile,
    component: Profile,
    meta: {
      auth: true
    }
  },
  {
    path: '/user',
    name: 'User',
    component: User,
    meta: {
      auth: true
    }
  },
  {
    path: '/user/:id/profile',
    name: 'UserProfile',
    component: UserProfile,
    meta: {
      auth: true
    }
  },
  {
    path:'/login',
    name: Login,
    component: Login 
  },
  {
    path:'/register',
    name: Register,
    component: Register 
  },
]

const router = new VueRouter({
  routes
})

export default router
