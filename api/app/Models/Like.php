<?php

namespace App\Models;
use App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public $table = "likes";

    protected $fillable = ['user_id', 'liked'];

    public function user(){
        return $this->belongTo(User::class);
    }

    public function liked(){
        return $this->morphTo();
    }
}
