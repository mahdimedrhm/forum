<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public $table = 'users';

    protected $fillable = [
        'fullname', 'login', 'email', 'password',
    ];
    
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
    
    public function followers(){
        return $this->belongsToMany(User::class, 'follower_user', 'user_id', 'follower_id');
    }

    public function followings(){
        return $this->belongsToMany(User::class, 'follower_user', 'follower_id', 'user_id');
    }

    public function likes(){
        return $this->hasMany(User::class);
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
