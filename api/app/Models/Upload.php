<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Model\Post;

class Upload extends Model
{
    public $table = 'uploads';

    protected $fillable = [
        'path', 'name', 'post_id'
    ];

    public function comments(){
        return $this->belongsTo(Post::class);
    }
}
