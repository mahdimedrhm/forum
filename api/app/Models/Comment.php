<?php

namespace App\Models;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Like;

class Comment extends Model
{
    public $table = 'comments';

    protected $fillable = [
        'content', 'post_id', 'user_id'
    ];

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->morphMany(Like::class, "liked");
    }
}
