<?php

namespace App\Models;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Upload;
use App\Models\Like;
use File;


class Post extends Model
{
    public $table = 'posts';

    protected $fillable = [
        'title', 'content', 'user_id'
    ];

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function uploads(){
        return $this->hasMany(Upload::class);
    }

    public function likes(){
        return $this->morphMany(Like::class, "liked");
    }

    protected static function boot()
    {
        parent::boot();
        Post::deleting(function($post) {
            foreach($post->uploads as $upload ) {
                File::delete(storage_path("/uploads/{$upload->path}"));
                $upload->delete();
            }
            foreach ($post->comments as $comment) {
                $comment->delete();
            }
        });
    }
}
