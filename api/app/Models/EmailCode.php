<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailCode extends Model
{
    public $table = 'emails_codes';
    
    protected $fillable = ['email', 'code'];
}
