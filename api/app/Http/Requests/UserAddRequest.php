<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fullname" => "required",
            "email" => "required|email|max:255|unique:users",
            "login" => "required",
            "password" => "required | min:8 | confirmed",
            "password_confirmation"
        ];
    }

    public function messages(){
        return [
            "fullname.required" => "the name is required",
            "email.required" => "the email is required",
            "email.email" => "you must give a valide email",
            "email.unique" => "this email is used before",
            "login.required" => "Login is required",
            "password.min" => "password too short"
        ];
    }
}
