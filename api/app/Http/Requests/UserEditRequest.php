<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "fullname" => "required",
            "email" => "required|email|max:255|unique:users,email," . $this->route()->user->id . ",id",
            "login" => "required",
            "oldPassword" => "required",
            "password" => "min:8 |confirmed"
        ];
    }

    public function messages(){
        return [
            "fullname.required" => "the name is required",
            "email.required" => "the email is required",
            "email.email" => "you must give a valide email",
            "email.unique" => "this email is used before",
            "login.required" => "Login is required",
            "password.min" => "Password is too short"
        ];
    }
}
