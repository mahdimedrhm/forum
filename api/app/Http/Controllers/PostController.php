<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Like;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Models\Upload;
use File;
use Illuminate\Support\Facades\Storage;


class PostController extends Controller
{
    
    public function index(Request $request)
    {
        $search= $request->search;
        if ($search == ''){
            return Post::with('user')->paginate(5);
        }else{
            return Post::where('title', 'like', '%'.$search.'%')->with('user')->paginate(5);
        }
    }

    public function store(PostRequest $request)
    {
        $post = new Post;
        $post->fill($request->only(["title", "content", "user_id"]));
        $post->save();

        foreach ($request->file('files', []) as $file) {
            $name = time() . "-" . $file->getClientOriginalName();
            $file->move(storage_path("/uploads"), $name);
            $files[]= [
                "post_id" => $post->id,
                "name" => $file->getClientOriginalName(),
                "path" => $name,
            ];
        }
    
        if (!empty($files)) {
            \DB::table('uploads')->insert($files);
        }
    
        return $post;
    }

    public function show(Post $post)
    {
        return $post->load('comments.user', 'comments.likes', 'user', 'uploads', 'likes');
    }

    
    public function update(PostRequest $request, Post $post)
    {
        $files = [];

        $uploadFiles = \DB::table('uploads')->where('post_id', $post->id)->get()->pluck('path')->toArray();
        $diffFiles = array_diff($uploadFiles, $request->get('oldFiles', []));
        $post->fill($request->only(["title", "content", "user_id"]));
        $post->save();

        foreach ($request->file("files", []) as $file) {
            $name = time() . "-" . $file->getClientOriginalName();
            $file->move(storage_path("/uploads"), $name);
            $files[]= [
                "post_id" => $post->id,
                "name" => $file->getClientOriginalName(),
                "path" => $name,
            ];
        }

        foreach ($diffFiles as $oldFile ) {
            Upload::where('path', $oldFile)->delete();
            File::delete(storage_path("/uploads/{$oldFile}"));
        }

        if (!empty($files)) {
            \DB::table('uploads')->insert($files);
        }
    }

    
    public function destroy(Post $post)
    {
        $post->delete();
    }

    public function showUpload(Upload $upload){
        $path = storage_path("/uploads".'/'. $upload->path);
        return response()->file($path);
    }

    public function like(Request $request, Post $post){
        $post->likes()->create(['liked_id'=> $post->id, 'user_id'=>$request->id]);
        $post->save();
    }

    public function unlike(Request $request, Post $post){
        $post->likes()->where('user_id', '=', $request->id)->delete();
    }

    public function userPosts($id){
        $posts = Post::where('user_id', '=', $id)->paginate(5);
        return $posts;
    }
}
