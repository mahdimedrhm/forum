<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\EmailCode;
use Illuminate\Http\Request;
use App\Http\Requests\UserAddRequest;
use App\Http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use File;

class UserController extends Controller
{
    public function index(Request $request){
        $search= $request->search;
        if ($search == ''){
            return User::with('posts')->paginate(5);
        }else{
            return User::with('posts')->where('fullname', 'like', '%'.$search.'%')
                        ->orWhere('email', 'like', '%'.$search.'%')
                        ->orWhere('login', 'like', '%'.$search.'%')
                        ->paginate(5);
        }
    }
    public function store(UserAddRequest $request){
        $image = $request->file('file');
        
        $user = new User;
        $user->fill($request->only(["fullname", "login", "email", "password"]));
        
        $name = time() . "-" . $image->getClientOriginalName();
        $image->move(storage_path("/users"), $name);
        $user->image_path = $name;
        
        $user->save();
    }

    public function update(UserEditRequest $request, User $user){
        if (Hash::check($request->oldPassword, $user->password)){
            if (!$request->changePassword){
                $user->fill($request->only(["fullname", "login", "email"]));
            }else {
                $user->fill($request->only(["fullname", "login", "email", "password"]));
            }
            $user->save();
        }
    }

    public function destroy(User $user){
        $user->delete();
    }

    public function show(User $user){
        return $user->load('followings', 'posts');
    }

    public function follow($userId, $followingId){
        User::find($userId)->followings()->attach(User::find($followingId));
    }

    public function unfollow($userId, $followingId){
        User::find($userId)->followings()->detach(User::find($followingId));
    }

    public function editImage(Request $request, User $user){
        $image = $request->file('file');
        $name = time() . "-" . $image->getClientOriginalName();
        $image->move(storage_path("/users"), $name);
        File::delete(storage_path("/users/{$user->image_path}"));
        
        $user->image_path = $name;
        $user->save();
    }

    public function showImage(User $user){
        $path = storage_path("/users".'/'. $user->image_path);
        return response()->file($path);
    }

    public function sendEmailCode(UserAddRequest $request){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $code = '';
        for ($i = 0; $i < 5; $i++) {
            $code .= $characters[rand(0, $charactersLength - 1)];
        }
        $emailCode = new EmailCode;
        $emailCode->email = $request->email;
        $emailCode->code = $code;
        Mail::to($emailCode->email)->send(new SendMailable($code));
        $emailCode->save();
    }

    public function resendEmailCode(Request $request){
        $emailCode = EmailCode::whereEmail($request->email)->first();
        Mail::to($emailCode->email)->send(new SendMailable($emailCode->code));

    }
    public function verifyEmail(Request $request){

        $emailCode = EmailCode::whereEmail($request->email)->first();
        if ($request->code == $emailCode->code){
            return true;
        }else{
            return false;
        }
    }

    public function userFollowings($id){
        $user = User::find($id)->followings()->paginate(5);
        return $user;
    }
}
