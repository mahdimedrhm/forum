<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Models\User;


class DashboardController extends Controller
{
    public function index(){
        $users = User::All();
        $userCount = count($users);
        $mostFollowedUsers = User::withCount('followers')->orderBy('followers_count', 'desc')->take(5)->get();

        $mostLikedPosts = Post::with('user')->withCount('likes')->orderBy('likes_count', 'desc')->take(5)->get();
        $posts = Post::All();
        $postCount = count($posts);

        $mostLikedComments = Comment::with('user')->withCount('likes')->orderBy('likes_count', 'desc')->take(5)->get();
        $comments = Comment::All();
        $commentCount = count($comments);

        return response()
            ->json(compact('mostFollowedUsers', 'userCount', 'mostLikedPosts', 'postCount', 'mostLikedComments', 'commentCount'));
    }
}
