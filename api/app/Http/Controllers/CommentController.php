<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{

    public function index()
    {
        return Comment::All();
    }

    public function store(CommentRequest $request)
    {
        $comment = new Comment;
        $comment->fill($request->only(['content','post_id' , 'user_id']));
        $comment->save();
    }

    public function show(Comment $comment)
    {
        return $comment->load("user");
    }

    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->fill($request->only(['content', 'post_id', 'user_id' ]));
        $comment->save();
    }

    
    public function destroy(Comment $comment)
    {
        $comment->delete();
    }

    public function like(Request $request, Comment $comment){
        $comment->likes()->create(['liked_id'=> $comment->id, 'user_id'=>$request->id]);
        $comment->save();
    }

    public function unlike(Request $request, Comment $comment){
        $comment->likes()->where('user_id', '=', $request->id)->delete();
    }
}
