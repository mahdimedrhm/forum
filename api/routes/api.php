<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::get('/user', 'UserController@index');
Route::get('/user/{user}', 'UserController@show');
Route::put('/user', 'UserController@store');
Route::put('/user/{user}', 'UserController@update');
Route::delete('/user/{user}', 'UserController@destroy');
Route::post('/user/{userId}/followes/{followingId}', 'UserController@follow');
Route::delete('/user/{userId}/followes/{followingId}', 'UserController@unfollow');
Route::get('/user/{user}/image','UserController@showImage');
Route::put('/user/{user}/image','UserController@editImage');
Route::get('/user/{id}/followings','UserController@userFollowings');


Route::get('/post', 'PostController@index');
Route::get('/post/{post}', 'PostController@show');
Route::post('/post', 'PostController@store');
Route::put('/post/{post}', 'PostController@update');
Route::delete('/post/{post}', 'PostController@destroy');
Route::get('/post/upload/{upload}', 'PostController@showUpload');
Route::post('/post/{post}/like', 'PostController@like');
Route::post('/post/{post}/like/delete', 'PostController@unlike');
Route::get('/post/user/{userId}', 'PostController@userPosts');


Route::get('/comment', 'CommentController@index');
Route::get('/comment/{comment}', 'CommentController@show');
Route::post('/comment', 'CommentController@store');
Route::put('/comment/{comment}', 'CommentController@update');
Route::delete('/comment/{comment}', 'CommentController@destroy');
Route::post('/comment/{comment}/like', 'CommentController@like');
Route::post('/comment/{comment}/like/delete', 'CommentController@unlike');

Route::get('/dashboard/index', 'DashboardController@index');


Route::post('/email/send', 'UserController@sendEmailCode');
Route::post('/email/verify', 'UserController@verifyEmail');
Route::post('/email/resend', 'UserController@resendEmailCode');


