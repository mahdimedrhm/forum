# Forum

This is aLpha's project to train on consentration and project management made with vue-cli3 and laravel

## Instalation

### The Backend (Laravel API)

1. Download the repository or clone the repository
```
git clone https://gitlab.com/mahdimedrhm/forum.git
```

2. Change the directory into api folder
```
cd api/
```

3. Install the dependencies by running Composer's install command
```
composer install
```
4. Create an environment file
```
cp .env.example .env
```
5. Edit `.env` and set your database connection details
6. Create a database named `forum
7. Edit `.env` to set mailtrap inbox
8. Migrate your database
```
php artisan migrate
```
9. Generate the application key.
```
php artisan key:generate
```
10. Set the `jwt-auth` secret by running the following command:
 ```
php artisan jwt:secret
```
11. run the seeds (password: "password") :
```
php artisan db:seed
```
### The frontend (vue-cli)
1. Change the directory into api folder
```
cd spa/
```
2. Get the node_modules:
```
yarn install or npm install
```

## Development

1. Run the local server:
```
cd api/
php artisan serve
```
2. Now click on the link shown in the terminal.

3. Vue:
```
cd spa/
yarn serve
```
4. Click on the link shown in the terminal.
